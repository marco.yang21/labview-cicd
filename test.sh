# use this to test locally

set -eou pipefail # needed so script exits on errors.
SECONDS=0

# This line reads in all the variables in the gitlab CI file and creates the appropriate env variables.
# This allows us to define the vars once in the yml file
# and then run the test process locally using the same vars
# see https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file
#. <(sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' vars.yml)
tmpfile=$(mktemp)
sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' 'vars.yml' > "$tmpfile"
. "$tmpfile"
rm "$tmpfile"
# We set the full version here to whatever is passed in
# This is done after we read in the yaml file so we overwrite whatever was there


CURRNE_PATH=""
CURRNE_PATH=$(pwd)
TEST_PATH="$CURRNE_PATH"/"$TEST_PATH"
TEST_REPORT_PATH="$CURRNE_PATH"/"$UNIT_TEST_REPORT_PATH"

# clean up any remnants from previous runs.
rm -rf "$TEST_REPORT_PATH"

# Run test cases
g-cli --lv-ver "$GCLI_LV_VERSION" "C:\\Program Files (x86)\\National Instruments\\LabVIEW 2020\\vi.lib\\addons\\_JKI Toolkits\\Caraya\\CarayaCLIExecutionEngine.vi" -- -s "$TEST_PATH" -x "$TEST_REPORT_PATH" -v 0
echo "Total Script Time: $SECONDS"
